"use strict";

/**
 * Translates protected CSS to vanilla CSS
 * The main feature of protected CSS is to add a root element reference to each
 * selector, to reduce openDOM custom components conflict with other elements.
 * It is a much less effective solution when compared to Vue or similar. It
 * can't prevent you to mess children style. But it is a great help for other
 * DOM branches and its ancestors.
 * @param {string} protectedCSS - the extended CSS code to be translated.
 * @param {string|false} rootDataId - the data-style-id value to reduce stiling mess.
 */
export function translateCSS(protectedCSS, rootDataId) {
  // TODO: do not mess media query!
  // TODO: do not mess @import!
  //protectedCSS.replace(/@root/g, `*[data-style-id="${rootDataId}"]`)
  const root = rootDataId ? `*[data-style-id="${rootDataId}"]` : ''
  return protectedCSS
  .trim()
  .replace(/\s+/gms, ' ')
  .replace(/\/\*[^*]*\*\//g, '')
  .replace(/\}\s*([^{}]+)/g, '}@@BR@@$1')
  .split('@@BR@@')
  .map(block => {
    const match = block.match(/^([^{]+)(\{.*\})$/)
    if (!match) return block
    let [ , selector, style ] = match
    selector = selector
    .split(/\s*,\s*/)
    .map(sel => {
      if (sel.indexOf('@root') === -1) {
        sel = '@root ' + sel.trim()
      }
      return sel.trim().replace(/@root/g, root)
    })
    .join(', ')
    return selector + style
  })
  .join('\n')
}
