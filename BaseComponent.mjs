"use strict";

import * as naiveML from '../naiveML/naiveML.mjs'
import { translateCSS } from './protected-css.mjs'

/**
 * Helper to crate a unique name based on the class name.
 * @param {function} klass - The component class reference.
 * @return {string} The randomized name of the tag to be used on the DOM.
 */
function classToTagName(klass) {
  return klass.name
    .replace(/[A-Z]/g, (chr)=> '-'+chr.toLocaleLowerCase())
    .replace(/^-/g, '') + '--' +
    Math.random().toString(36).split('.')[1]
}

/**
 * Basic class to be extended by independant components.
 */
export default class BaseComponent extends window.HTMLElement {

  /**
   * Assync loads a component.
   * @param {string} uri - the location of the component module with default
   *                       exporting a class with BaseComponent interface.
   * @param {object} shared - a colection of shared objects
   * @return {Promise} a class with BaseComponent interface
   */
  loadComponent(uri, shared) {
    throw Error('This method must to be defined by the component loader.')
  }

  /**
   * If you wont use the `create` method to create instances to the DOM, or
   * you want a friendly name to write some HTML code, you must use this method
   * to define the custom element to the document. Otherwise, forget this method.
   * @param {string} name - the tag name you want.
   */
  static declareCustomElement(name) {
    if (!name.trim()) throw Error('Bad tag name.')
    // TODO: test name colisions.
    this.__tagName = name.trim()
    customElements.define(this.__tagName, this)
  }

  /**
   * Retrieve the component tag name.
   * @return {string} a name that you can use in the DOM.
   */
  static get tagName() {
    if (!this.__tagName) this.declareCustomElement(classToTagName(this))
    return this.__tagName
  }

  /**
   * Creates a element instace to be added to the DOM.
   * This method should be used by the component loader.
   * @param {Object} attributes - attributes to be applyed on this component
   * and other configuration options. Special keys:
   * @param {Element} attributes.parent - append the new instance to that element.
   * @param {string} attributes.text - add some text to the new instance.
   * @return {HTMLElement} the new instance
   */
  static __create(attributes = {}) {
    const el = document.createElement(this.tagName)
    queueMicrotask(() => {
      Object.entries(attributes).forEach(([attr, value])=> {
        if  (attr === 'parent') value.appendChild(el)
        else if (attr === 'text') el.innerText = value
        else el.setAttribute(attr, value)
      })
      el.__afterCreate()
    })
    return el
  }

  __afterCreate() {}

  /**
   * This placeholder must to be implemented by the child class.
   * It can also be replaced by a simple attribute `style` with a
   * string value, representing a CSS code.
   */
  style() {
    return '/* No stylesheet defined */'
  }

  /**
   * This placeholder must to be implemented by the child class.
   * It can also be replaced by a simple attribute `template` with a
   * string value, representing a naiveML code.
   */
  template() {
    return 'strong text="No template defined ☹️"'
  }

  /**
   * Updates the inner component style element with the providen css code
   * or apply the this.style value or result.
   */
  updateStyle(cssCode) {
    console.log('updateStyle', this.tagName)
    const style = this.__root.querySelector('style')
    if (!cssCode) cssCode = this.style.call ? this.style() : this.style
    style.textContent = translateCSS(cssCode, this.__elementStyleId)
  }

  /**
   * Creates the instance. You must not use this directly.
   * Use the `create` class method (or the `document.createElement`,
   * if you know what are you doing).
   *
   * If you need to change this parameters on a component, set it on `super()`
   * @param {Object} opts - options to set the behaviour of this component
   * @param {string} opts.mode - the `attachShadow()`'s mode parameter, defaults to "closed".
   * @param {boolean} opts.delegatesFocus - the `attachShadow()`'s delegatesFocus parameter.
   * @param {boolean} opts.openDOM - If true it will not attach the template to shadowDOM,
   *                                 but direct to this and you can set childrem elements
   *                                 like moust elements, otherwise the inner dom is protected
   *                                 and childrem elements are ignored.
   *                                 If you set opts.openDOM to true, pay atention to your
   *                                 style to do not mess with other elements in the page.
   * The `opts.mode` will be ignored and setted to "open" when the you attach
   * the `forceOpenShadow` to the class. It is very useful for testing.
   */
  constructor(opts) {
    super()
    let { mode, delegatesFocus, openDOM } = {
      mode: 'closed',
      delegatesFocus: true,
      openDOM: false,
      ...opts
    }
    if (this.constructor.forceOpenShadow) mode = 'open' // Usefull for testing
    this.__elementStyleId = openDOM && Math.random().toString(36).split('.')[1]
    this.__root = openDOM ? this : this.attachShadow({mode, delegatesFocus})
  }

  get root() {
    return this.__root
  }

  connectedCallback() {
    console.log('After Connected', this.tagName)
    if (this.__elementStyleId) {
      this.__root.dataset.styleId = this.__elementStyleId
    }
    this.redraw()
    if (this.afterConnected) this.afterConnected()
  }

  async redraw() {
    const naiveMLCode = this.template.call ? await this.template() : this.template
    const innerDOM = naiveML.build(naiveMLCode, {
      componentId: null, /*TODO*/
      loader: (url)=> this.loadComponent(url).then(component => component.create())
    })
    while (this.__root.firstChild) this.__root.removeChild(this.__root.firstChild)
    const style = document.createElement('style')
    this.__root.appendChild(style)
    this.__root.appendChild(innerDOM)
    this.updateStyle()
  }
}
